import fastkiss
from httpcore import hasKey

when isMainModule:
  let app = newApp()
  app.config.address = "127.0.0.1"
  app.config.port = 9080

  app.get("/ip", proc (req: Request) {.async.} =
    if hasKey(req.headers, "remote_addr"):
      respond req.headers["remote_addr"]
    else:
      respond "unknown"
  )

  app.run()
