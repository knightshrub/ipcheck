# Package

version       = "0.1.0"
author        = "knightshrub"
description   = "Return the clients public IP address"
license       = "GPL-3.0-or-later"
srcDir        = "src"
bin           = @["ipcheck"]


# Dependencies

requires "nim >= 1.4.8"
requires "fastkiss"
